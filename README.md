# Backend-Base for NodeJS Projects.

### What is this repository for?

- This repository is for making robust and container-based backend applications.
- This one specifically uses Node.JS and Express as its core.

### Features

- JEST integration with test examples.
- MVC skeleton with working examples.
- Database connection, specifically PostgreSQL.
- Eslint text correction.
- BitBucket-based pipeline for use on any kind of VPS or cloud-based server with SSH authentication.
- Pipeline with 4 stages: Test, Development, Staging and Production.

### How to set-up?

- Install latest version of Docker and Docker-Compose.
- Make 4 .env files called .env.test, .env.dev, .env.stg and .env.prod.
- Put the 4 .env files in the config folder.
- Change your values based on .env.example.
- Run

  > docker-compose **--env-file ./config/.env.stg** up -d --build

  > The --env-file directory is what .env file you will be reading for the database initialization.

- Change the base code and you're done!