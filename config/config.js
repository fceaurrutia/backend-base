const path = require("path");
require("dotenv").config({
  path: path.join(__dirname, `./.env.${process.env.NODE_ENV}`)
});
module.exports = {
  dev: {
    username: process.env.APP_DB_USER,
    password: process.env.APP_DB_PASS,
    database: process.env.APP_DB_NAME,
    host: process.env.APP_DB_HOST,
    dialect: "postgresql"
  },
  stg: {
    username: process.env.APP_DB_USER,
    password: process.env.APP_DB_PASS,
    database: process.env.APP_DB_NAME,
    host: process.env.APP_DB_HOST,
    dialect: "postgresql"
  },
  test: {
    username: process.env.APP_DB_USER,
    password: process.env.APP_DB_PASS,
    database: process.env.APP_DB_NAME,
    host: process.env.APP_DB_HOST,
    dialect: "postgresql"
  },
  prod: {
    username: process.env.APP_DB_USER,
    password: process.env.APP_DB_PASS,
    database: process.env.APP_DB_NAME,
    host: process.env.APP_DB_HOST,
    dialect: "postgresql"
  }
};
