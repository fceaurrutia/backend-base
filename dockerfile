FROM node:14-alpine
ARG ENV
ENV ENV ${ENV}
WORKDIR /backend
COPY . /backend
COPY package.json /backend
RUN rm -rf node_modules
RUN npm install
CMD npm run env:${ENV}