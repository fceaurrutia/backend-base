// Middleware Imports.
const express = require("express");
const app = express();
const path = require("path");
const envVars = require("dotenv").config({
  path: path.join(__dirname, `./.env.${process.env.NODE_ENV}`)
});
const routing = require("./src/routes");
require("./src/database/database.js");
const cors = require("cors");
const { errorHandler } = require("./src/middleware/errorHandler");
const { languageHandler } = require("./src/middleware/languageHandler");

// Middleware application.
app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "PATCH", "DELETE"]
  })
);

app.use(errorHandler);
app.use(languageHandler);
app.use(express.json());
app.use(routing);

// app.listen(port, function () {
//   console.log("Aplicación funcionando correctamente en el puerto " + port);
//   console.log("CORS: " + process.env.CORS);
// });

module.exports = app;
